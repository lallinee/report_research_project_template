"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_lorenz.py
```
"""
from pathlib import Path

import numpy as np


import inspect
import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.lorenz.solver import Simul

src_file_path = inspect.getfile(lambda: None)
here = Path(src_file_path).absolute().parent


params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.02
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs0 + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys0 - 1.0)
sim.state.state_phys.set_var("Z", sim.Zs0 - 10.0)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()
# see also the other plot_* methods of sim.output.print_stdout
fig_dir = here.parent / "fig"

plt.savefig(fig_dir / "fig_simlorenz.png")
plt.show()
