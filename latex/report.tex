\documentclass[a4paper,12pt]{article}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts}
\usepackage{amssymb}
\usepackage{float}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{lmodern}
\usepackage{textcomp}

\usepackage{natbib}

\usepackage[top=1.2in,bottom=1.2in,left=3.cm,right=3.cm,a4paper]{geometry}

\usepackage[font=footnotesize]{caption}

\usepackage{xcolor}

\usepackage{hyperref}
\makeatletter
\let\thetitle\@title
\let\theauthor\@author
\let\thedate\@date
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!99!black}
}
\setlength{\parindent}{0cm}
\usepackage{titling}

\pretitle{%
\includegraphics[width=0.2\linewidth]{../fig/logo_UGA}
\vspace{2cm}
\begin{center}
\LARGE
}
\posttitle{\end{center}}
\postdate{\par\end{center}\vspace{12cm}~}

\usepackage[nottoc,numbib]{tocbibind}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Instabilities and Turbulences}
\author{Ewen LALLINEC}

% to define new commands
\newcommand{\R}{\mathcal{R}}
\newcommand{\mean}[1]{\langle #1 \rangle}

% to show the pieces of advice
\newcommand{\advice}[1]{{\it #1}}
% to hide the pieces of advice
% \newcommand{\advice}[1]{}

\begin{document}

\renewcommand{\labelitemi}{$\bullet$}

\begin{titlepage}
    \begin{minipage}{0.5\textwidth}
		\begin{flushleft} \includegraphics[width=0.8\textwidth]{../fig/logo_UGA.png}
			\end{flushleft}
			\end{minipage}
			\begin{minipage}{0.5\textwidth}
            
			\begin{flushright} \includegraphics[width=0.5\textwidth]{../fig/phitem.png}
		\end{flushright}
        
	\end{minipage}\\[2.5 cm]
    \begin{center}
       
        \textsc{\huge Research report} \\[2.5cm]
   
    
	\rule{\linewidth}{0.2 mm} \\[1 cm]
	{ \huge \bfseries \thetitle}\\[.7 cm]
	\rule{\linewidth}{0.2 mm} \\[2 cm]
	{\large UFR PHITEM, UGA}\\[.2cm]
	{\Large \thedate}\\[2cm]
    \end{center}

    \begin{minipage}{0.5\textwidth}
		\begin{flushleft} \large
		    \textbf{Author }\\
		    Ewen Lallinec\
		\end{flushleft}
		
	\end{minipage}
	\begin{minipage}{0.5\textwidth}
        \begin{flushright} \large
			\textbf{Supervisor} \\
			Pierre Augier
		\end{flushright}
	\end{minipage}

\end{titlepage}

\tableofcontents

\newpage

\section{Introduction}
In this report we will study different dynamical systems that can lead to instabilities and we will study when those instabilities can happen and how the system will behave following those instabilities.

\section{Simple dynamical systems}
In this first part we will study two low-dimensioned dynamical models: the Lotka-Volterra model and the Lorenz model. They both are ordinary differential systems and describe the evolution of multiple variables through time.

\subsection{Lotka-Volterra predator-prey model}

The Lotka-Volterra is a 2D prey-predator model which was first designed in 1926 to model and understand the increase of the population of predator fishes in the Adriatic sea during the WWI. The model is the following:
\\

\begin{equation}
\begin{cases}
	\dot{X}& = AX- BXY \\
	\dot{Y}& = -CY + DXY\\
\end{cases}
\ \ \ A, B, C, D >0
\label{eq:prey}
\end{equation}


\begin{itemize}
	\item X(t) represents the population of prey at time t
	\item Y(t) represents the population of predator at time t
	\item A is the reproduction rate for the prey  without considering the number of predator
	\item B is the death rate for the prey because of the predators
	\item C is the death rate for the predator without considering the prey eaten
	\item D is the reproduction rate of the predator considering the prey and how many of them were eaten \cite{Volterra}
\end{itemize}

For the first equation the first term consider the birth of prey without predator then the second term introduces the predator action on the population of prey with a negative term as when the number of predator increases, the number of prey eaten gets bigger and thus the prey population decreases.
\\

For the second equation by analogy with the first one the first term represents the predator dying due to the absence of preys, then the second term represents the prey eaten and thus the increase in predator birth (indeed when the number of prey increases, more are eaten thus more predator are born).

\subsubsection{Fixed points}
We are looking for the fixed points of this system. Which means the points $(X,Y)$ such as the predator number and the prey number remains constant through time  it gives us the following system.


\[
\begin{cases}
0& = AX- BXY \\
0& = -CY + DXY\\
\end{cases}
\]


With those 2 equations we easily find the fixed points to be $\displaystyle{(0,0)}$ and $\displaystyle{\Big(\frac{C}{D},\frac{A}{B}\Big)}$. 
The first one being logical, if there are no preys nor predators there won't be any after any time t.

Moreover we can check if our fixed points are stable or not.  To do so all we need is considering a small variation around our fixed points.
\\

If we write $(X_f,Y_f)$ our couples of fixed points we can write near those fixed points $(X,Y)=(X_f +x, Y_f+y)$ with $x<<X_f$ and $y<<Y_f$.
\\

If we inject this in our system and linearize it (we get rid of second order terms in x and y) we get :

\[
\begin{cases}
\dot{x}& = (X_f+x)(A- BY_f) -BX_fy \\
\dot{y}& = (Y_f+y)(-C+ DX_f) + DY_fx\\
\end{cases}
\]
\\

For our first fixed point $(X,Y)=(0,0)$ we have:


\[
\begin{cases}
\dot{x}& = Ax \\
\dot{y}& = -Cy \\
\end{cases}
\]


Thus the Jacobian of our system is:
\[ J=
\begin{pmatrix}
A & 0 \\
0 & -C \\
\end{pmatrix}
\]

And we directly get our eigenvalues and the associated eigenvectors:
\[\begin{matrix}
\lambda_1 = A, & e_1 = \begin{pmatrix}
1\\
0
\end{pmatrix} \\ \\
\lambda_2 = -C, & e_2 = \begin{pmatrix}
0\\
1
\end{pmatrix}
\end{matrix}
\]

As the real part (which is the eigenvalue) of $\lambda_1=A$ is positive we get that our fixed point is unstable. Moreover it is a saddle point.

Then with the same method we can see if our second fixed point $\displaystyle{\Big(\frac{C}{D},\frac{A}{B}\Big)}$ is stable, we get the following system:

\[
\begin{cases}
\dot{x}& = -\frac{CB}{D}y \\
\dot{y}& =  \frac{AD}{B}x\\
\end{cases}
\]

Thus the Jacobian of the system is:
\[ J=
\begin{pmatrix}
0 & -\frac{CB}{D} \\
\frac{AD}{B} & 0 \\
\end{pmatrix}
\]
\\

We can easily compute our eigenvalues and the associated eigenvectors:
\[\begin{matrix}
\lambda_1 =i\sqrt{AC} , & e_1 = \begin{pmatrix}
i\frac{B\sqrt{C}}{D\sqrt{A}}\\
1
\end{pmatrix} \\ \\
\lambda_2 =-i\sqrt{AC} , & e_2 = \begin{pmatrix}
-i\frac{B\sqrt{C}}{D\sqrt{A}}\\
1
\end{pmatrix}
\end{matrix}
\]

As both the real parts of our eigenvalues are null we get that our fixed point is elliptic.

Moreover we can show that there is a quantity F that is constant through time :
\[F:(X,Y) \mapsto C\log(X) − DX + A\log(Y) − BY \]

By derivating F we have :
\[\dot{F}(X,Y) = C \frac{\dot{X}}{X} - D\dot{X} + A\frac{\dot{Y}}{Y}-B\dot{Y} \]

Using our system \ref{eq:prey} we have:
\[ 
\dot{F}(X,Y) = \Big(AX-BXY\Big)\Big(\frac{C}{X}-D\Big) + \Big(-CY+DXY \Big)\Big(\frac{A}{Y}-B\Big)
\]

Thus we have \[\dot{F}(X,Y) =0 \]

F is indeed constant through time.

\subsubsection{Numerical solutions}
We will write $X_0$ and $Y_0$ as the coordinates of our initial point.

First we represented the evolution of population with default parameters: A=1, B=1, C=1 and D=0.5.

So according to our calculations our fixed point should be $(X,Y)=(2,1)$. 


\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{../fig/fig_simpredaprey2.png}
	\caption{Isoline of potential for the prey-predator model with $(X_0,Y_0)=(2,1)$}
	\label{fig:def_iso}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{../fig/fig_simpredaprey1.png}
	\caption{Evolution of X and Y through time with $(X_0,Y_0)=(2,1)$}
	\label{fig:def_evo}
\end{figure}

We see with our simulation that our calculations were right, (2,1) is a fixed point, as the number of preys and predators is constant through time.

\begin{figure}[H]
    \centering
    \includegraphics[scale =0.6]{../fig/fig_isocontour.png}
    \caption{Isocontour of prey-predator model}
    \label{fig:isocontour}
\end{figure}

Choosing other starting points for our model we get more classical isolines of potential for our system.

\subsection{Lorenz Model}
The Lorenz system is a 3D dynamic system that can be under some assumption chaotic. Meaning that when the initial condition vary just a bit the whole system is changed. Indeed the atypical form of the points trajectories is what inspired the expression "Butterfly effect". This model was created to model the convection in the atmosphere.
\begin{equation}
\begin{cases}
	\dot{X}& =\sigma (Y-X) \\
	\dot{Y}& = \rho X - Y - XZ\\
	\dot{Z}&=XY-\beta Z
\end{cases}
\ \ \ \sigma, \beta, \rho >0
\label{eq:lorenz}
\end{equation}


\begin{itemize}
	\item X(t), Y(t), Z(t) represent the state of the system at each time t \cite{Lorenz}
	\item $\sigma$ is the Prandtl number\cite{Lorenz}
	\item $\rho$ is the quotient of the Rayleigh number with the critical Rayleigh number, representing the convection \cite{Lorenz}
	\item $\beta$ is a parameter depending on the physical scale and parameters of the layer studied \cite{Lorenz}
\end{itemize}

\subsubsection{Fixed points}
We are looking for the fixed points of this system i.e the points (X,Y,Z) such that :
\[
\begin{cases}
	0& =\sigma (Y-X) \\
	0& = \rho X - Y - XZ\\
	0&=XY-\beta Z
\end{cases}
\]

There is one immediate fixed point which is $(0,0,0)$. Let's check if there are any other different fixed points. We can change the form of our system :
\[
\begin{cases}
	X& =Y\\
	0& = X(\rho  - 1 - Z)\\
	Z&= \frac{X^2}{\beta}
\end{cases}
\]

We can't have $X=Y=0$ as we would have again $Z =0$. So we have $X\neq 0$thus $\rho - 1-Z=0$ which means that $\rho$ must be over 1 as $\displaystyle{Z=\frac{X^2}{\beta} >0}$. 

Then under the assumption that $\rho > 1$ we have two new fixed points : 

\begin{center}
$(\sqrt{\beta(\rho -1)},\sqrt{\beta(\rho -1)},\rho -1)$ and $(-\sqrt{\beta(\rho -1)},-\sqrt{\beta(\rho -1)},\rho -1)$.
\end{center}


Now let's study the stability of our fixed points, we place ourselves near our fixeds points ($X=X_f +x$, $Y=Y_f +y$, $Z=Z_f +z$) and linearize our equation:

\[ 
\begin{cases}
	\dot{x}& =\sigma (Y_f+y-X_f-x) \\
	\dot{y}& = \rho(X_f+x) - (Y_f+y) - X_f(Z_f+z) -Z_f(X_f +x)\\
	\dot{z}&=X_f(Y_f+y) +Y_f(X_f +x)-\beta (Z_f+z)
\end{cases}
\]
Thus around the point $(0,0,0)$ we have:

\[ 
\begin{cases}
	\dot{x}& =\sigma (y-x) \\
	\dot{y}& = \rho x - y \\
	\dot{z}&=-\beta z
\end{cases}
\]

The Jacobian of our system is :

\[ J=
\begin{pmatrix}
-\sigma & \sigma & 0\\
\rho & -1 & 0 \\
0 & 0 & -\beta 
\end{pmatrix}
\]

We get the 3 eigenvalues of our system:
\[
\begin{matrix}
\lambda_1 = -\beta \\
\lambda_2 = \frac{-(\sigma +1) -\sqrt{(\sigma +1)^2+4\sigma(\rho -1)}}{2}\\

\lambda_3 = \frac{-(\sigma +1) +\sqrt{(\sigma +1)^2+4\sigma(\rho -1)}}{2}
\end{matrix}
\]
If $\rho<1$ we get that all the real parts of our eigenvalues are negative, thus our fixed point is stable.
But if $\rho >1$ we have $\lambda_3>0$ thus our system is unstable (1 degree of unstability), moreover our fixed point is a saddle point.

Next we consider the two other fixed points, we can get our characteristic polynomial:
\[\Pi(\lambda)= \lambda^3 +\lambda^2(\sigma +\beta +1) \lambda \beta (\rho + \sigma) + 2\beta \sigma(\rho -1) \]

If we fix $\sigma =10$, $\beta = \frac{8}{3}$ (as Lorenz historically did when he studied this system), we get different conditions of unstabilities following the value of $\rho$.

\begin{itemize}
    \item $1<\rho \leq 1.3456$ we have three negative eigenvalues thus the fixed points are stable
    \item $1.3456 <\rho$ \begin{itemize}
        \item if $\rho <24.74$ the real part of our 3 eigenvalues is negative so the fixed points are still stable
        \item if $\rho \approx 24.74$ we have real parts of our eigenvalues which are all null, so we have an elliptic point
        \item if $\rho >24.74$ the complex eigenvalues have positive real part so the fixed points are unstable and it is a saddle point
        \item we can remark that for $\rho =28$, the system is chaotic
    \end{itemize}
\end{itemize}

\subsubsection{Numerical results}
\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.5]{../fig/fig_simlorenz.png}
    \caption{Lorenz attractor}
    \label{fig:lorenz}
\end{figure}
We numerically get the butterfly shape for classic values of our parameters, we can distinctively see our two fixed points around whose the trajectories are fixed.

\section{Another dynamical system: Competitive Lotka-Volterra equations}
The Lotka-Volterra prey-predator system is well known but another system was also designed by them and is often used: the Competitive Lotka-Volterra equations.

First it can be seen in 2D as the Prey-Predator system: 

\begin{equation}
\begin{cases}
    \dot{X_1}=&r_1 X_1(1-\frac{X_1+\alpha_{12}X_2}{K_1}) \\
    \dot{X_2}=&r_2 X_2(1-\frac{X_2+\alpha_{21}X_1}{K_2})
\end{cases}
\end{equation}

\begin{itemize}
    \item $X_1$ and $X_2$ represent the two populations in competition
    \item $r_i$ represents the growth rate of the i-th specie
    \item $\alpha_{ij}$ represents the effect of population j on population j, as it is competitive those values are always positive. If they are both negative, it becomes mutualism and if they are of opposite signs the systems becomes parasitism or predation. We fix $a_{ii}$ as 1 to have a more realistic model.
    \item $K_i$ is the carrying capacity of population i representing the maximum possible number of individuals in the population i in the considered environment
\end{itemize}

As we saw before we can easily describe the competition between 2 species but what becomes interesting is the fact that we can describe following the same principle the competition between N populations if we consider the following 

\begin{equation}
    \dot{X_i} =r_i X_i\Big(1-\frac{\sum\limits_{k=1}^N\alpha_{ik}X_k}{K_i}\Big) \ ,\ 1\leq i\leq N
\end{equation}

We can also look for fixed points in those equation :
\begin{equation}
    0 =r_i X_i\Big(1-\frac{\sum\limits_{k=1}^N\alpha_{ik}X_k}{K_i}\Big) \ ,\ 1\leq i\leq N
\end{equation}

The trivial fixed point is still $(0,\dots,0)$, but there are other fixed points: 
\[ X_{if} =K_i - \sum_{\underset{k\neq i}{k=1}}^N\alpha_{ik}X_k  \ ,\ 1\leq i\leq N\]

We can study the stability of those points. For the trivial point we get (if we write $X_i = X_{if} + x_i$):


\[ \dot{x_i} = r_ix_i\Big(1-\frac{\sum\limits_{k=1}^N\alpha_{ik}x_k}{K_i}\Big)  \ ,\ 1\leq i\leq N\]

However if we linearize we get:
\[\dot{x_i} = x_i \].

Meaning that our eigenvalues are all 1. So our trivial fixed point is unstable and is a saddle point.
\\

For the other fixed point we get the Jacobian:

\[J = \begin{pmatrix}
r_1 -1 & \alpha_{12} & \dots & \alpha_{1n} \\
\vdots & \ddots & \dots& \vdots \\
\vdots & \dots & \ddots & \vdots \\
\alpha_{n1} & \dots &\alpha_{n (n-1)} & r_n -1
\end{pmatrix} \]

We get a N-degree charateristic polynomial which is too hard to solve, that's where other stability study method are useful such as Lyapunov functions come in hand. However using those functions we can theoretically show that these fixed points can be stable under certains assumptions about the values of the $\alpha_{ij}  \ ,\ 1\leq i,j\leq N$ and we would get an attractor.

To show this we can see this figure from the work of  Vano \cite{Vano}
\begin{figure}[H]
    \centering
    \includegraphics[scale =0.4]{../fig/competitive.png}
    \caption{4-Dimension competitive system with $X_4$ plotted as colors}
    \label{fig:competitive}
\end{figure}
\bibliographystyle{plain}
\bibliography{biblio}

\end{document}

