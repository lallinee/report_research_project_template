# A latex template for reports on research projects

This repository contains files to produce a pdf file for the report of a
research project with LaTeX (pdflatex and bibtex), Python and a Makefile.

To setup the environment to compile the pdf, follow the instructions in the
file
[install.md](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence/-/blob/master/install.md).

### Create your onw repository and start working on it locally

Fork this repo
https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/report_research_project_template.
You should have a new repo
https://gricad-gitlab.univ-grenoble-alpes.fr/myusername/report_research_project_template.

Clone your own repository with the program ``hg``:

```bash
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/myusername/report_research_project_template.git
```

#### Temporary hack (2020-11)

With hg-git 0.9.0 (get the version with ``hg version -v``), there is a bug with
push via https. This bug can be overcame by adding the login and password to
the path. Edit the file `.hg/hgrc` in the root of your local repository with
the command `gedit .hg/hgrc &` (Do not forget to save the file!).

Replace the line
```
default = https://gricad-gitlab.univ-grenoble-alpes.fr/myusername/report_research_project_template.git
```
by
```
default = https://myusername:password@gricad-gitlab.univ-grenoble-alpes.fr/myusername/report_research_project_template.git
```

### Compile to produce the pdf document

Finally, to compile the pdf, use the command `make`. (You can have a look at
the Makefile to understand what it does.)

This should produce a pdf file `report.pdf` that can be viewed for example with
the command `evince report.pdf &`.

You have to make sure that the pdf is produced from a clean directory. Try this:

```bash
make cleanall
make
```

### Description of the main files

The main files are:

- `README.rst`: file containing this text.

- `Makefile`: describes how to build the pdf; is used by the program
  `make`.

- In the directory `latex`:

  - `report.tex`: file containing the latex code.
  - `biblio.bib`: file containing the bibliography entries.
  - `jfm.bst`: file containing the style for the bibliography.

- In the directory `fig` (all figures that are not produced by scripts):

  - `logo_UGA.png`: the logo of the university.

- The directory `python` contains python scripts to make figures and tables.

### Note for the teacher
I added the report on the gitlab but you can suppress and the make still works, moreover I changed python to python3 in the makefile such that it would work on my computer, maybe it will cause trouble to you if you don't have python3.
